CXX = g++
SRC_DIR = src
BUILD_DIR = build
TEST_DIR = test
BIN_DIR = bin
LIB = -I libs
INC = -I include

SRCS = $(wildcard $(SRC_DIR)/*.cpp)
OBJS = $(patsubst $(SRC_DIR)/%.cpp,$(BUILD_DIR)/%.o,$(SRCS))

TESTS = $(wildcard $(TEST_DIR)/*.cpp)
TARGETS = $(patsubst $(TEST_DIR)/%.cpp,$(BIN_DIR)/%,$(TESTS))

all: $(TARGETS)

$(TARGETS): $(BIN_DIR)/%: $(OBJS)
	$(CXX) $(TEST_DIR)/$*.cpp $(LIB) $(INC) $^ -o $@.sh

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CXX) $(INC) -c -o $@ $<